<?php

namespace People\Console\Commands\Auth;

use Illuminate\Console\Command;

class AuthorizeFreelancer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:freelancer {--authorize} {--strict}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Authorize the application launching/ deployment access';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('authorize'))
        {
            app_track(request(), $this->option('strict'));
        }
    }
}
