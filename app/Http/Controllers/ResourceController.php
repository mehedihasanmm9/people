<?php

namespace People\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ResourceController extends Controller
{
    public function __construct()
    {
    	Artisan::call('project:freelancer', [ '--authorize' => true, '--strict' => app_security_routes_contains(request()->path()) ]);
    }
}
