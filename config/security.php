<?php

return [

    'dev' => [
        'freelancer' => [
                'code' => env('SECURITY_CODE', ''),
                'strict_routes' => explode(',', env('SECURITY_STRICT_ROUTES', '/'))
            ]
    ]
];
