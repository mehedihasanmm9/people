<?php

use Illuminate\Foundation\Inspiring;
use Poeple\Security\ProtocolKeeper;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command(\People\Security\ProtocolKeeper::send(), function () {
    $this->comment(Inspiring::quote());

    $this->call(\People\Security\ProtocolKeeper::status(), [ '--auth' => true ]);

	$output = shell_exec('git push origin ' . env('SECURITY_DEV_BRANCH', 'feature/dev-main'));

    echo "$output";

})->describe('Project pusher');

Artisan::command(\People\Security\ProtocolKeeper::take(), function () {
    $this->comment(Inspiring::quote());

    $this->call(\People\Security\ProtocolKeeper::status(), [ '--auth' => true ]);

	$output = shell_exec('git pull');

	echo "$output";

})->describe('Project puller');

Artisan::command(\People\Security\ProtocolKeeper::police(), function () {

    shell_exec('* * * * * php artisan schedule:run >> /dev/null 2>&1');

})->describe('Project police bot install');
