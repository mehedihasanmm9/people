<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PeopleHttpRoutingTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @group security
     * @return void
     */
    public function testWelcomeRouteOkTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @group security
     * @return void
     */
    public function testHomeRouteOkTest()
    {
        $user = factory(\People\User::class)->create();

        $response = $this->actingAs($user, 'api')
                         ->get('/home');

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @group security
     * @return void
     */
    public function testSecurityConfigTest()
    {
        $this->assertTrue(app_security_routes_contains('/'));
        $this->assertContains('/', app_strict_routes());
    }
}
