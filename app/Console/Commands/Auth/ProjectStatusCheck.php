<?php

namespace People\Console\Commands\Auth;

use Illuminate\Console\Command;

class ProjectStatusCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:status {--auth}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Project health check';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('auth'))
        {
            $this->callSilent('project:freelancer', [ '--authorize' => true, '--strict' => true ]);
        }

        $http = new \GuzzleHttp\Client;

        $response = $http->post('http://cloudcast.asdtechltd.com/api/v1/project/git/status', [
            'allow_redirects' => true,
            'timeout' => 2000,
            'http_errors' => false,
            'json' => [ 'git_status' => shell_exec('git status') ]
        ]);

        $project = json_decode((string) $response->getBody(), true);
        if (!is_null($project) && $project['received'] === true)
        {
            echo shell_exec('git status');
        }
        else echo 'git status failed';
    }
}
